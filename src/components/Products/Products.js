import axios from 'axios'
import { useEffect, useState } from 'react'
import ProductDetail from '../Detail/ProductDetail'
import Form from '../Form/Form'
import Product from './Product'
import classes from './Products.module.scss'

const Products = (props) => {

    const [keyword, setKeyword] = useState('')
    const [state, setState] = useState([])
    const [search, setSearch] = useState([])
    const [showDetail, setShowDetail] = useState(false)
    const [showForm, setShowForm] = useState(false)
    const [selectedProduct, setSelectedProduct] = useState()
    const [status, setStatus] = useState(0);

    useEffect(() => {
        const getList = async () => {
            const req = await axios.get("http://127.0.0.1:5000/products")            
            // const req = await axios.get("https://jsonplaceholder.typicode.com/posts")            
            setState({
                ...state,
                products: req.data.products
            });
            setSearch({
                ...state,
                products: req.data.products
            })
        }
        getList();
    }, [])

    const searchProduct = (e) => {
        setKeyword(e.target.value);
        const searchedProducts = search.products
            .filter(x => x.product_name.toLowerCase()
                .match(e.target.value && e.target.value.toLowerCase()));

        setState(prevState => ({
            ...prevState,
            products: searchedProducts
        }))
    }

    const showProductHandler = (idProduct) => {
        const selected = state.products.find(x => x.product_id === +idProduct);
        setSelectedProduct(selected);
        setShowDetail(true);
    }

    const closeHandler = () => {
        setShowDetail(false);
    }

    const deleteHandler = (productName) => {
        const doDel = async () => {
            const req = await axios.delete("http://127.0.0.1:5000/product/"+productName);
            setStatus(Math.random());
        }
        doDel();
    }

    return (
        <>
            <div className={classes.container}>

                <input className={[classes.input].join(' ')}
                    value={keyword}
                    type="text"
                    placeholder="Cari by nama"
                    onChange={(e) => searchProduct(e)}
                />

                <button 
                    className="btn-add" 
                    onClick={() => setShowForm(true)}>+</button>

                {showForm ? (<Form
                    setShowForm={setShowForm}
                    setStatus={setStatus}
                />) : ('')}

                <h3>List of Products from Api(s)</h3>
                
                <div className={classes.wrapper}>
                    {state.products &&
                        state.products.sort((a, b) => b.product_id - a.product_id)
                        .map((product) => {
                            return (
                                <Product
                                    id={product.product_id}
                                    showProduct={showProductHandler}
                                    deleteHandler={deleteHandler}
                                    product={product} />
                            )
                        })}
                </div>

                {showDetail ? (
                    <ProductDetail
                        close={closeHandler}
                        product={selectedProduct} />
                ) : ''}
            </div>
        </>)
}

export default Products
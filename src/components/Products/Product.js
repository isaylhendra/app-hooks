import React from 'react';
import NumberFormat from 'react-number-format';
import { Link } from 'react-router-dom';
import '../UI/Card.scss';

const Product = (props) => {

    return (
        <>
            <div className="card"
            // onClick={() => props?.showProduct(props.product?.name)}
            >
                <img className={"img-circle"}
                    width="100px"
                    src={props.product?.product_image ? props.product?.product_image : 'https://d1mb9nzi8kqzgo.cloudfront.net/images/products/ee3880b1-abfd-42b3-b424-f086754b397c.jpg'}
                    alt={props.product?.name} />
                <p>ID: {props?.id}</p>
                <h1>{props.product?.product_name}</h1>
                <NumberFormat value={props.product?.product_price}
                    displayType={'text'}
                    thousandSeparator={true}
                    prefix={'Rp. '}
                    renderText={value => <div>{value}</div>} />
                <p>{props.product?.product_description}</p>

                {props.product?.product_id > 10 ? (
                    <button
                        className={"btn-del"}
                        onClick={() => props.deleteHandler(props.product?.product_name)}>X</button>
                ) : ''}
                <button
                    className={"btn-add"}
                // onClick={() => props.showProduct(props.product?.product_id)}
                >
                    <Link to={{
                        pathname: "/detail",
                        query: props.product
                    }}>View</Link>
                </button>
            </div>
        </>)
}

export default Product
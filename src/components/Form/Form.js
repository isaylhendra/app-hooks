import axios from 'axios'
import { useState } from 'react'

const Form = (props) => {
    const [state, setState] = useState({
        product_name: '',
        product_description: '',
        product_price: 0,
    })
    const [status, setStatus] = useState(0)

    const wrapper = {        
        position: "fixed",
        left: "0"
        , right: "0"
        , width: "100%"
        , height: "100%"
        // , border: "solid 1px black"
        , alignItems: "center"
        , display: "flex"        
        , justifyContent: "center"
    }

    const style = {
        border: 'solid 1px grey'
        , padding: '20px'
        , display: "flex"
        , flexDirection: "column"
        , width: "350px"
        , justifyContent: "center"
        , background: '#fff'
        , boxShadow: "1px 4px 4px rgba(0,0,0,0.2)"
        , borderRadius: "10px"
        , position: "absolute"
        , top: "0"
    }

    const handleChange = (e) => {
        const { name, value } = e.target;
        setState({
            ...state,
            [name]: value
        })
    }

    const saveHandler = () => {
        const data = new FormData();
        data.append('product_name', state.product_name)
        data.append('product_description', state.product_description)
        data.append('product_price', state.product_price)
        const doPost = async () => {
            const req = await axios.post(
                "http://127.0.0.1:5000/product",
                data
            );
            props.setStatus(Math.random());           
            setStatus(req.status)
        }
        doPost();
    }

    return (
        <>
            <div style={wrapper}>
                <div style={style}>

                    <h3>Form Input (Without Validation)</h3>

                    {status && status == 200 ? 
                        (<h5>Data Successfully Insert</h5>) 
                            : status === 500 ? (<strong>Error: {status}</strong>) : ''}
                    <div>
                        <label>Product Name</label>
                        <input className={"input"}
                            name="product_name"
                            type="text"
                            value={state.product_name}
                            onChange={(e) => handleChange(e)}
                            placeholder="Input Product Name"
                        />
                    </div>
                    <div>
                        <label>Product Description</label>
                        <input className={"input"}
                            name="product_description"
                            type="text-area"
                            value={state.product_description}
                            onChange={(e) => handleChange(e)}
                            placeholder="Input Product Description"
                        />
                    </div>
                    <div>
                        <label>Product Price (Rp.)</label>
                        <input className={"input"}
                            name="product_price"
                            type="text"
                            value={state.product_price}
                            onChange={(e) => handleChange(e)}
                            placeholder="Input Product Price"
                        />
                    </div>
                    <br />
                    <div>
                        <button
                            onClick={() => (saveHandler())}
                            className={"btn-submit"}>Save</button>
                        <button
                            onClick={() => (props.setShowForm(false))}
                            className={"btn-submit"}>Cancel</button>
                    </div>
                </div>
            </div>
        </>)
}

export default Form
import classes from './Input.module.scss'

const InputBlue = (props) => {

    return (
        <>
            <input className={classes.input}
                    type="text"
                    placeholder="input here"
                />
        </>)
}

export default InputBlue
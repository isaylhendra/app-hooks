import './Detail.scss'
import { withRouter } from 'react-router'

const ProductDetail = (props) => {

    const { product_description
        , product_id
        , product_image
        , product_name
        , product_price } = props.location.query
    console.log(props);

    return (
        <>
            <div className={"footer"}>
                <h3>Detail Product: {product_description && product_name} {product_price}</h3>
                <div className={"detail"}>
                    <button className="close" onClick={props.close}>X</button>
                    <img src={props.product?.product_image ? props.product?.product_image : 'https://d1mb9nzi8kqzgo.cloudfront.net/images/products/ee3880b1-abfd-42b3-b424-f086754b397c.jpg'}
                        alt={props.product?.product_name} width="30%" />
                    <ul className="listing">
                        <li><p>{props.product?.product_id}</p></li>
                        <li><h3>{props.product?.product_name}</h3></li>
                        <li><p>description: {props.product?.product_description}</p></li>
                    </ul>
                </div>
            </div>
        </>)
}
export default ProductDetail
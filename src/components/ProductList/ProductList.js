import axios from 'axios'
import { useEffect, useState } from 'react'
import Person from '../../components/Products/Product'

const ProductList = (props) => {
    const [state, setState] = useState({
        products: []
    })

    useEffect(() => {
        const getList = async () => {
            // const req = await axios.get("https://jsonplaceholder.typicode.com/posts");
            const req = await axios.get("http://127.0.0.1:5000/products");
            setState({
                products: req.data.products
            });
        }
        getList();        
    }, [])

    return (
        <>
            <h2>Listing ini diambil dari API</h2>
            <div style={{
                display: "flex", 
                flexWrap: "wrap", 
                justifyContent: "center" }}>
                {/* {JSON.stringify(state)} */}
                {state && state.products.map((data, index) => (
                    <Person
                        key={index}
                        id={data.product_id}
                        product={data}
                    />
                ))}
            </div>
        </>)
}

export default ProductList
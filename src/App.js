import './App.scss';
import Products from './components/Products/Products';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router'
import ProductDetail from './components/Detail/ProductDetail';

function App() {
  return (
    <Router basename={"/"}>
      <div className="App">
        <h1>Applikasi Untuk Belajar React Hooks</h1>
        {/* <Products /> */}
        {/* <ProductList/> */}
        <Switch>
          <Route path="/" exact component={Products} />
          <Route path="/detail" component={ProductDetail} />
        </Switch>

      </div>
    </Router>
  );
}

export default App;
